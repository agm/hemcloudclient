#!/bin/bash

#Script to check if there is internet connectivity 
#  Ping details 
# -q quiet
# -c no of pings
# -w timeout

sleep 10

IP='8.8.8.8'
ping -c5 -w10 $IP 2>/dev/null 1>/dev/null
if [ "$?" = 0 ]
then
  #if [ -e /var/log/gsmLog.log 
  sudo su -c "echo 'INFO: $(date -Iseconds). Internet connectivity exists'>> /var/log/gsmDebug.log"
  exit
else
  sudo su -c "echo 'INFO: $(date -Iseconds). No Internet'>> /var/log/gsmDebug.log"

  #Check if the ppp0 interface is up
  t1=$(ifconfig | grep -o ppp0)
  t2='ppp0'

  if [ "$t1" = "$t2" ]
  then
    sudo su -c "echo 'WARNING: $(date -Iseconds). No internet but ppp0 interface is up'>> /var/log/gsmDebug.log"
    sudo su -c "echo 'WARNING: $(date -Iseconds). Stopping cellConfig'>> /var/log/gsmDebug.log"
    sudo poff cellConfig
    sleep 5
    sudo su -c "echo 'WARNING: $(date -Iseconds). Starting cellConfig'>> /var/log/gsmDebug.log"
    sudo pon cellConfig 
    sleep 10
  else
    sudo su -c "echo 'WARNING: $(date -Iseconds). ppp0 interface is down'>> /var/log/gsmDebug.log"
    sudo su -c "echo 'WARNING: $(date -Iseconds). Starting cellConfig'>> /var/log/gsmDebug.log"
    sudo pon cellConfig
    sleep 10
  fi

fi

#Sleep for 200s and check if it works 
sleep 200

sudo su -c "echo 'WARNING: $(date -Iseconds). Checking internet connectivity again'>> /var/log/gsmDebug.log"

IP='8.8.8.8'
ping -c5 -w10 $IP 2>/dev/null 1>/dev/null
if [ "$?" = 0 ]
then
  #if [ -e /var/log/gsmLog.log 
  sudo su -c "echo 'INFO: $(date -Iseconds). Checking again. Internet connectivity exists'>> /var/log/gsmDebug.log"
else
  sudo su -c "echo 'INFO: $(date -Iseconds). Checking again. No Internet. Restarting GSM module'>> /var/log/gsmDebug.log"
  #Stopping cellConfig
  sudo poff cellConfig
  #restart the GSM through GPIO
  sudo su -c "echo 66 > /sys/class/gpio/export; echo out > /sys/class/gpio/gpio66/direction; echo 1 > /sys/class/gpio/gpio66/value; echo 0 > /sys/class/gpio/gpio66/value; echo 66 > /sys/class/gpio/unexport"
  sleep 200

  #Check if the ppp0 interface is up
  t1=$(ifconfig | grep -o ppp0)
  t2='ppp0'

  if [ "$t1" = "$t2" ]
  then
    sudo su -c "echo 'INFO: $(date -Iseconds). ppp0 interface is up'>> /var/log/gsmDebug.log"
  else
    sudo su -c "echo 'WARNING: $(date -Iseconds). ppp0 interface is down'>> /var/log/gsmDebug.log"
    sudo su -c "echo 'WARNING: $(date -Iseconds). Starting cellConfig'>> /var/log/gsmDebug.log"
    sudo poff cellConfig
    sleep 10
    sudo pon cellConfig
    sleep 10
  fi
fi

#start the ppp client
#sudo pon cellConfig

exit
