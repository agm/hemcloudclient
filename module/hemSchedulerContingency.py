#
# Class for managing a schedule on one HEM

# Author: David Sehloff
# Updated by Ashray Manur

import datetime
import threading
import time
from hemClient import HemClient

#startTime=datetime.datetime.now()

class HemScheduleContingencyMgr(object):
    def __init__(self, priority):
        self.priority = priority
        self.nodes = []
        self.__numLoads = 8
        self.defaultLoads()
        self.initSchedule()
        self.nodeState = {node: None for node in self.nodes}
        self.__updateInterval=1
        self.__correctionInterval=datetime.timedelta(minutes=10)
        #self.__minOnDiff={node: None for node in self.__nodes}
        #self.__minOffDiff={node: None for node in self.__nodes}
        self.scheduleChange = False
        self.__lastActuation = None

    def initSchedule(self):
        onTimes=[]
        offTimes=[]
        i=1
        while i <= self.__numLoads:
            onTimes.append([])
            offTimes.append([])
            i += 1
        self.__schedule = {'on':dict(zip(self.nodes, onTimes)), 'off':dict(zip(self.nodes, offTimes))}

    def appendOnTime(self,node, onTime):
        self.__schedule['on'][node].append(onTime)
        self.__schedule['on'][node].sort()

    def appendOffTime(self,node, offTime):
        self.__schedule['off'][node].append(offTime)
        self.__schedule['off'][node].sort()

    def extendOnTime(self,node, onTimeList):
        self.__schedule['on'][node].extend(onTimeList)
        self.__schedule['on'][node].sort()

    def extendOffTime(self,node, offTimeList):
        self.__schedule['off'][node].extend(offTimeList)
        self.__schedule['off'][node].sort()
    
    def removeOnTime(self, node, onTime):
        self.__schedule['on'][node].remove(onTime)
    
    def removeOffTime(self, node, offTime):
        self.__schedule['off'][node].remove(offTime)

    def setUpdateInterval(self, seconds):
        '''
        Set update interval of schedule executor in seconds
        '''
        self.__updateInterval=seconds
        
    def setCorrectionInterval(self, delta):
        '''
        Set correction interval of schedule executor as a timedelta
        '''
        self.__correctionInterval=delta

    def getSchedule(self):
        return self.__schedule

    def getLoads(self):
        return self.nodes
        
    def getLoadState(self):
        return self.nodeState

    def defaultLoads(self):
        self.nodes = []
        i=0
        while i < self.__numLoads:
            self.nodes.append('l'+str(i))
            i += 1

    def setLoad(self, nodeNum, newName):
        if (newName not in self.nodes):
            try:
                self.nodes[nodeNum]=newName
            except IndexError:
                print('setLoad could not change name of node with index '+str(nodeNum))
        else:
            print('Name given to setLoad is already in use. No change was made.')



    def setStates(self):
            self.scheduleChange = False
            for node, onSchedule in self.__schedule['on'].items():
                dateTimeNow = datetime.datetime.now()
                minOnDiff=None
                minOffDiff=None
                for onItem in onSchedule:
                    difference = dateTimeNow-onItem
                    #Break if iteration has reached scheduled time in the future
                    if(difference.total_seconds() < 0):
                        break
                    #Save current difference if iteration is at a scheduled time that is in the past. Remove time item from list.
                    else:
                        minOnDiff=difference
                        #if (len(onSchedule)>2):
                            #self.removeOnTime(node, onItem) #remove onItem from list
                offSchedule =  self.__schedule['off'][node]
                for offItem in offSchedule:
                    difference = dateTimeNow-offItem
                    #Break if iteration has reached scheduled time in the future
                    if(difference.total_seconds() < 0):
                        break
                    #Save current difference and time item if iteration is at a scheduled time that is in the past. Remove time item from list.
                    else:
                        minOffDiff=difference
                        
                        #if (len(offSchedule)>2):
                            #self.removeOffTime(node, offItem) #remove offItem from list
                    #Update min difference if less than previous diff and positive (scheduled time is in the future)
        

                if(minOnDiff is None):
                    if(minOffDiff is None):
                         self.nodeState[node]='NA'
                    else:
                        #if(self.nodeState[node]!='off'):
                        self.nodeState[node]='off'
                        self.scheduleChange = True
                else:
                    if(minOffDiff is None):
                        #if(self.nodeState[node]!='on'):
                        self.nodeState[node]='on'
                        self.scheduleChange = True
                    elif(minOnDiff<minOffDiff):
                        #if(self.nodeState[node]!='on'):
                        self.nodeState[node]='on'
                        self.scheduleChange = True
                    else:
                         #if(self.nodeState[node]!='off'):
                        self.nodeState[node]='off'
                        self.scheduleChange = True

            # Return the time at which the state dict was last updated
            #updateTime = dateTimeNow

 


