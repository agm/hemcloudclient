"""
Author 1: Ashray Manur
    Email: ashraymanur@gmail.com

    MQTT Transport implementation for connecting HEMv3.X modules to Elektrifi Cloud. It internally uses Python Paho library.
"""


import yaml
import sys
import os
import atexit
import logging
import errno
import signal
import time
import threading
import urllib2
import ssl
import json
import uuid
import paho.mqtt.client as mqtt



class HemCloudClient:

    """
    Client to connect with Elektrifi Cloud. This is the variation of the original HemCloudClient
    written for Kanana and other field deployments. For SOLEEN. 
    """

    def __init__(self, access_token, host, address_mapping_actuation):
        """
        Todo:
        * Implicit initialization has to be changed to user defined. For example, server address, certificates etc.
        * User defined init has to be done through a YAML file.
        * Things to be obtained from config file are as follows:
        * Security credentials
        * Server address
        * Device info - type of HEM
        * Data rate - low/high depending on the type of connectivity
        * Node assignments - what is connected to which node
        * Max system voltage

        """
        #self.configFile = False
        self.connected_flag = False
        self.topic_pub = 'v1/devices/me/telemetry'
        self.ACCESS_TOKEN = access_token
        self.host = host
        self.messageCounter = 0
        self.logger = logging.getLogger(__name__)
        self.address_mapping_actuation = address_mapping_actuation
        self.publisher = HemPublisher()

        self.cloudClient = mqtt.Client()
        self.cloudClient.username_pw_set(self.ACCESS_TOKEN)
        self.cloudClient.max_queued_messages_set(5000)
        self.in_flight = 10
        # self.cloudClient.tls_set(ca_certs="mqttserver.pub.pem", certfile="mqttclient.nopass.pem", keyfile=None, cert_reqs=ssl.CERT_REQUIRED,
        # tls_version=ssl.PROTOCOL_TLSv1, ciphers=None);

        # self.cloudClient.tls_insecure_set(False)

        self.cloudClient.on_connect = self.on_connect
        self.cloudClient.on_disconnect = self.on_disconnect
        #self.cloudClient.on_publish = self.on_publish

        self.cloudClient.connect(self.host,1883,60)
        #self.cloudClient.loop_start()
        #self.cloudClient.loop_start()
        #self.cloudClient.loop_start()

        #t = threading.Thread(target=self.checkBrokerConnection)
        t = threading.Thread(target=self.run)
        t.start()

    def run(self):
        while True:
            self.cloudClient.loop_forever()
            time.sleep(0.005)


    def subscribe(self, callback):
        functionId = str(uuid.uuid4())
        self.publisher.register(functionId, callback)

    def on_connect(self, client, userdata, flag, rc):
        """
        Called when the broker responds to our connection request.

        Args:
            client:     the client instance for this callback
            userdata:   the private user data as set in Client() or userdata_set()
            flags:      response flags sent by the broker
            rc:         the connection result
        """
        if rc == 0:
            self.connected_flag = True
            #print "MQTT connection established"
            self.logger.info("Cloud MQTT connection success")
        else:
            self.connected_flag = False
            #print "MQTT connection failed"
            self.logger.warning("Cloud MQTT connection fail")
        pass

    def on_disconnect(self, client, userdata, rc):
        """
        Invoked when disconnected from broker.  Two scenarios are possible:
        1) Broker rejects a connection request
        2) Client initiated disconnect

        Args:
            client: The client instance for this callback
            userdata: The private user data as set in Client() or userdata_set()
            rc: The connection result
        """
        #print "Cloud disconnected"
        if rc != 0:
            self.cloudClient.loop_stop()
            self.connected_flag = False
            #print "Unexpected disconnection from cloud"
            self.logger.info('Unexpected disconnection from cloud')
        else:
            self.cloudClient.loop_stop()
            self.connected_flag = False
            #print "Disconnected from cloud"
            self.logger.warning("Disconnected from cloud")
        pass

    def on_publish(self, client, userdata, mid):
        """
        Called when publish transmission is completed.

        Args:
            client: The client instance for this callback
            userdata: The private user data as set in Client() or userdata_set()
            mid: Message ID
        """
        #self.logger.debug("Message Delivered: {0}".format(str(mid)))
        print "Data published"

    def sendData(self, message):
        """
        Todo:
        * Assumes system has only two loads. It has to be made more geenric

        """
        if(message['TYPE'] == 'STATUS'):
            #print "Status"
            val = [float(i) for i in message['VALUE']]
            sendMsg = {
                "Charger Status": val[self.address_mapping_actuation["CHG"]],
                "PV 1 Status ": val[self.address_mapping_actuation["PV1"]],
                "PV 2 Status ": val[self.address_mapping_actuation["PV2"]],
                "Load 1 Status": val[self.address_mapping_actuation["LOD1"]],
                "Load 2 Status": val[self.address_mapping_actuation["LOD2"]]}
            payload = json.dumps(sendMsg, sort_keys=True, indent=5)
            self.cloudClient.publish(self.topic_pub, payload, qos=2)

        if(message['TYPE'] == 'SOC'):
            #print "SOC"
            ts = int(time.time())
            val = message['VALUE']
            sendMsg = {"SoC": val}
            payload = json.dumps(sendMsg, sort_keys=True, indent=1)
            self.cloudClient.publish(self.topic_pub, payload, qos=2)
            #print "sent battery voltage data\n"

        if(message['TYPE'] == 'DCVOLT'):
            #print "DCCURRENT"
            val = [float(i) for i in message['VALUE']]
            sendMsg = {
                "Battery Voltage": val[self.address_mapping_actuation["BAT"]]}
            payload = json.dumps(sendMsg, sort_keys=True, indent=1)
            self.cloudClient.publish(self.topic_pub, payload, qos=2)

        if(message['TYPE'] == 'DCCURRENT'):
            #print "Status"
            val = [float(i) for i in message['VALUE']]
            sendMsg = {
                "Charger Current": val[self.address_mapping_actuation["CHG"]],
                "PV1 Current": val[self.address_mapping_actuation["PV1"]],
                "PV2 Current": val[self.address_mapping_actuation["PV2"]],
                "Load 1 Current": val[self.address_mapping_actuation["LOD1"]],
                "Load 2 Current": val[self.address_mapping_actuation["LOD2"]]}
            payload = json.dumps(sendMsg, sort_keys=True, indent=5)
            self.cloudClient.publish(self.topic_pub, payload, qos=2)

        if(message['TYPE'] == 'DCPOWER'):
            #print "Status"
            val = [float(i) for i in message['VALUE']]
            sendMsg = {
                "Charger Power": val[self.address_mapping_actuation["CHG"]],
                "PV1 Power": val[self.address_mapping_actuation["PV1"]],
                "PV2 Power": val[self.address_mapping_actuation["PV2"]],
                "Load 1 Power": val[self.address_mapping_actuation["LOD1"]],
                "Load 2 Power": val[self.address_mapping_actuation["LOD2"]]}
            payload = json.dumps(sendMsg, sort_keys=True, indent=5)
            self.cloudClient.publish(self.topic_pub, payload, qos=2)


class HemPublisher:

    def __init__(self):
        self.subscribers = dict()

    def register(self, who, callback=None):
        if callback is None:
            callback = getattr(who, 'update')
            print callback
        self.subscribers[who] = callback

    def unregister(self,who):
        del self.subscribers[who]

    def dispatch(self,message):
        for subscriber, callback in self.subscribers.items():
            callback(message)
