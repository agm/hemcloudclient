# hemSocCalculator.py
# This module is used to calculate state of charge initially and at any later point of time.
# Author: Maitreyee Marathe

from module.hemSuperClient import HemSuperClient
import sys
import time

sys.path.insert(1, '../')


class stateOfChargeCalc(object):

    def __init__(
            self,
            batteryCapacity,
            voltageMax,
            voltageMin,
            stateOfChargeMax,
            stateOfChargeMin,
            sourceNodes,
            loadNodes):
        # batteryCapacity in Ah
        # voltageMax (in V) = voltage corresponding to stateOfChargeMax (in %)
        # voltageMin (in V) = voltage corresponding to stateOfChargeMin (in %)
        # sourceNode = list of node numbers corresponding to sources/import
        # loadNodes = list of node numbers corresponding to loads/export
        self.__batteryCapacity = batteryCapacity
        self.__voltageMax = voltageMax
        self.__voltageMin = voltageMin
        self.__stateOfChargeMax = stateOfChargeMax
        self.__stateOfChargeMin = stateOfChargeMin
        self.__sourceNodes = sourceNodes
        self.__loadNodes = loadNodes
        self.__batteryVoltage = 0.0
        self.__initialSoC = 0.0
        self.__initialSourceCharge = 0.0
        self.__initialLoadCharge = 0.0
        self.__charges = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        self.__hemSuperClient = HemSuperClient("localhost", 9931)
        # Subscribe to data from HEMs
        # The argument to this is the name of the function you want triggered
        # when you get data
        self.__hemSuperClient.subscribe(self.update)


    def initialStateOfCharge(self):
        """Calculates the initial state of charge based on voltage. Should be called when the script starts.
        Turns off all the nodes during the measurement, does not restore them to their previous state.
        """
        for i in range(8):
            self.__hemSuperClient.sendRequest("/api/turnoff/" + str(i))
        time.sleep(3)
        self.__hemSuperClient.sendRequest("/api/getdcvoltage/all")
        time.sleep(0.1)
        #print 'self.__batteryVoltage', self.__batteryVoltage
        #print 'self.__loadNodes[0]', self.__loadNodes[0]
        stateOfCharge = (self.__batteryVoltage - self.__voltageMax) * (self.__stateOfChargeMax -
                                                                       self.__stateOfChargeMin) / (self.__voltageMax - self.__voltageMin) + self.__stateOfChargeMax
        self.__initialSoC = stateOfCharge
        return stateOfCharge

    def initialCharges(self):
        self.__hemSuperClient.sendRequest("/api/getdccharge/all")
        time.sleep(2)
        for i in self.__sourceNodes:
            self.__initialSourceCharge = self.__initialSourceCharge + \
                self.__charges[i]
        for i in self.__loadNodes:
            self.__initialLoadCharge = self.__initialLoadCharge + \
                self.__charges[i]

    def stateOfCharge(self):
        SourceCharge = 0
        LoadCharge = 0
        self.__hemSuperClient.sendRequest("/api/getdccharge/all")
        time.sleep(0.1)
        for i in self.__sourceNodes:
            SourceCharge += self.__charges[i]
        for i in self.__loadNodes:
            LoadCharge += self.__charges[i]
        deltaCharge = (SourceCharge - self.__initialSourceCharge) - \
            (LoadCharge - self.__initialLoadCharge)
        SoC = self.__initialSoC + \
            ((deltaCharge * 100) / (self.__batteryCapacity * 3600))
        return SoC

    def update(self, message, address):
        """ This is function which gets triggered whenever you get data from server
        You can add more logic here for post processing
        {u'NODE': u'ALL', u'TYPE': u'DCPOWER', u'VALUE': [0.185, 5.9, 85.6, 10.4, 0, 0, 0, 12.5]} ('192.168.1.236', 9931)
        `message` is a list which gives you the type of response and the corresponding nodes
        `address` is a tuple giving you the server address and the port
        """
        #print message
        if(message['TYPE'] == 'DCVOLT' and message['NODE'] == 'ALL'):
            self.__batteryVoltage = message.values()[2][self.__loadNodes[0]]

        if(message['TYPE'] == 'DCCHARGE' and message['NODE'] == 'ALL'):
            self.__charges = message.values()[2]
