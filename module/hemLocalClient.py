"""
Author 1: Ashray Manur
    Email: ashraymanur@gmail.com

    MQTT Transport implementation for local message exchange. It internally uses Python Paho library.
"""


import yaml
import sys
import os
import atexit
import logging
import errno
import signal
import time
import threading
import urllib2
import ssl
import json
import uuid
import paho.mqtt.client as mqtt
from hemCloudClient import HemCloudClient

class HemLocalClient:

    """
    Client to connect with Elektrifi Cloud. This is the variation of the original HemCloudClient
    written for Kanana and other field deployments. For SOLEEN. 
    """

    def __init__(self, topic, address_mapping_actuation):

        self.connected_flag = False
        self.host = "localhost"
        self.localClient = mqtt.Client()
        #self.cloudClient = HemCloudClient()
        self.publisher = HemPublisher()

        # Add message callbacks that will only trigger on a specific subscription match.
        #self.localClient.message_callback_add("$SYS/broker/messages/#", on_message_msgs)
        #self.localClient.message_callback_add("$SYS/broker/bytes/#", on_message_bytes)

        self.address_mapping_actuation = address_mapping_actuation
        self.topic = topic
        self.localClient.on_connect = self.on_connect
        self.localClient.on_disconnect = self.on_disconnect
        self.localClient.on_publish = self.on_publish
        self.localClient.on_message = self.on_message
        #self.localClient.on_subscribe = self.on_subscribe
        self.localClient.max_queued_messages_set(50)
        self.localClient.in_flight = 10
        self.localClient.connect(self.host,1883,60)

        if(self.topic != None ):
            print "subscribing"
            self.localClient.subscribe(self.topic)

        self.logger = logging.getLogger(__name__)
        t = threading.Thread(target=self.run)
        t.start()

    #def on_subscribe(self, client, userdata, mid, granted_qos):  # subscribe to mqtt broker
    #    print("Subscribed", userdata)

    def run(self):
        while True:
            self.localClient.loop_forever()
            time.sleep(0.005)

    def subscribe(self, callback):
        functionId = str(uuid.uuid4())
        self.publisher.register(functionId, callback)


    def on_connect(self, client, userdata, flag, rc):
        """
        Called when the broker responds to our connection request.

        Args:
            client:     the client instance for this callback
            userdata:   the private user data as set in Client() or userdata_set()
            flags:      response flags sent by the broker
            rc:         the connection result
        """

        if rc == 0:
            self.connected_flag = True
            print "MQTT connection established"
            self.logger.warning("Local MQTT connection success")
        else:
            self.connected_flag = False
            self.logger.warning("MQTT connection failed")
            self.logger.warning("Local MQTT connection fail")
        pass

    def on_disconnect(self, client, userdata, rc):
        """
        Invoked when disconnected from broker.  Two scenarios are possible:
        1) Broker rejects a connection request
        2) Client initiated disconnect

        Args:
            client: The client instance for this callback
            userdata: The private user data as set in Client() or userdata_set()
            rc: The connection result
        """
        #print "Cloud disconnected"
        self.localClient.loop_stop()
        if rc != 0:
            self.connected_flag = False
            #self.logger.info('Unexpected disconnection from cloud')
        else:
            self.connected_flag = False
            #self.logger.warning("Disconnected from cloud")
        pass

    def on_publish(self, client, userdata, mid):
        """
        Called when publish transmission is completed.

        Args:
            client: The client instance for this callback
            userdata: The private user data as set in Client() or userdata_set()
            mid: Message ID
        """
        #self.logger.debug("Message Delivered: {0}".format(str(mid)))
        #print "Data published"
        #print result

    def on_message(self, client, userdata, msg):
        """
        When client receives a message this is called

        """
        #print "Message rec"
        #print(msg.topic + "-" + str(msg.qos) + "-" + str(msg.payload))
        self.publisher.dispatch(msg)

    def sendData(self, message):
        #print "sending data"
        if(message['TYPE'] == "SOC"):
            #message_temp = {"SOC": message['VALUE']}
            payload = json.dumps(message, sort_keys=True, indent=1)
            self.localClient.publish("local/soc", payload, qos=2)

        if(message['TYPE'] == "DCVOLT"):
            #message_temp = {"SOC": message['VALUE']}
            payload = json.dumps(message, sort_keys=True, indent=1)
            self.localClient.publish("local/dcvolt", payload, qos=2)


    def on_subscribe(self, client, userdata, mid, granted_qos):
        print("Subscribed: "+str(mid)+" "+str(granted_qos))

class HemPublisher:

    def __init__(self):
        self.subscribers = dict()

    def register(self, who, callback=None):
        if callback is None:
            callback = getattr(who, 'update')
            print callback
        self.subscribers[who] = callback

    def unregister(self,who):
        del self.subscribers[who]

    def dispatch(self,message):
        for subscriber, callback in self.subscribers.items():
            callback(message)
