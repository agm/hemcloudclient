#!/bin/bash

line="0 * * * * /home/hem/hemcloudclient/cleanHemLog.sh"

if ! crontab -l | grep -q '/home/hem/hemcloudclient/cleanHemLog.sh'; then
	echo "Adding cronjob"
	(crontab -u hem -l; echo "$line" ) | crontab -u hem -
else 
	echo "Cronjob already exits"
fi