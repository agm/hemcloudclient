#!/bin/bash

#Script to check if there is internet connectivity 
# -q quiet
# -c no of pings
# -w timeout

sleep 10

IP='8.8.8.8'
ping -c5 -w10 $IP 2>/dev/null 1>/dev/null
if [ "$?" = 0 ]
then
  #if [ -e /var/log/gsmLog.log 
  sudo su -c "echo 'INFO: $(date -Iseconds). Internet present'>> /var/log/gsmDebug.log"
else
  sudo su -c "echo 'INFO: $(date -Iseconds). No Internet'>> /var/log/gsmDebug.log"

  #Check if the ppp0 interface is up
  t1=$(ifconfig | grep -o ppp0)
  t2='ppp0'

  if [ "$t1" = "$t2" ]
  then
    sudo su -c "echo 'Time: $(date -Iseconds). ppp0 interface is up'>> /var/log/gsmDebug.log"
    sudo poff cellConfig
    sleep 5
    sudo pon cellConfig
    sleep 10
    ping -c5 -w10 $IP 2>/dev/null 1>/dev/null
    if [ "$?" = 0 ]
    then
      #restart the GSM through GPIO
      sudo su -c "echo 66 > /sys/class/gpio/export; echo out > /sys/class/gpio/gpio66/direction; echo 1 > /sys/class/gpio/gpio66/value; echo 0 > /sys/class/gpio/gpio66/value; echo 66 > /sys/class/gpio/unexport"


  else
    sudo su -c "echo 'Time: $(date -Iseconds). ppp0 interface is down'>> /var/log/gsmDebug.log"
    sudo pon cellConfig
    sleep 10
  fi

fi

#restart the GSM through GPIO
#sudo su -c "echo 66 > /sys/class/gpio/export; echo out > /sys/class/gpio/gpio66/direction; echo 1 > /sys/class/gpio/gpio66/value; echo 0 > /sys/class/gpio/gpio66/value; echo 66 > /sys/class/gpio/unexport"

#sleep 30

#start the ppp client
#sudo pon cellConfig

exit
