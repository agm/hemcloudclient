#!/bin/bash

sudo cp /home/hem/hemcloudclient/hem_cloud.service /lib/systemd/system/
sudo cp /home/hem/hemcloudclient/hem_cellular.service /lib/systemd/system/
sudo cp /home/hem/hemcloudclient/monitrc /etc/monit/

sudo systemctl enable hem_cloud.service
sudo systemctl enable hem_cellular.service

sleep 5
sudo reboot
