"""
Cloud
======
This code is for sending and receiving data from the cloud platform for field installations
Author: Ashray Manur
"""

import datetime
import threading
import time
import sys
import random
import logging
import yaml
import signal
import paho.mqtt.client as mqtt


# For the modules
sys.path.insert(1, '../')

from module.hemSuperClient import HemSuperClient
from module.hemCloudClient import HemCloudClient

"""
Create a new HEM Client. This is used to send and receive data from your HEM
It takes two arguments - IP/hostname and port
If you're deploying your app on the same HEM you're getting data from use localhost.Otherwise give IP address
Port is usually 9931
"""
hemSuperClient = HemSuperClient("localhost", 9931)

hemCloudClient = None 

#global variables
address_mapping_scheduler = {}
address_mapping_actuation = {}
access_token = ""
server_address = ""

hem_status = 0
hem_current = 0
hem_voltage = 0
hem_power= 0


# logging schedule and all events
logging.basicConfig(
    filename='cloud.log',
    level=logging.DEBUG,
    format='%(asctime)s %(message)s',
    datefmt='%m/%d/%Y %I:%M:%S %p')

# Logger Object
logger = logging.getLogger(__name__)

# Subscribe to data from HEMs
# The argument to this is the name of the function you want triggered when
# you get data
def hemUpdate(message, address):
    """ This is function which gets triggered whenever you get data from server
    You can add more logic here for post processing
    {u'NODE': u'ALL', u'TYPE': u'DCPOWER', u'VALUE': [0.185, 5.9, 85.6, 10.4, 0, 0, 0, 12.5]} ('192.168.1.236', 9931)
    `message` is a list which gives you the type of response and the corresponding nodes
    `address` is a tuple giving you the server address and the port
    """
    global hem_status
    global hem_current
    global hem_voltage
    global hem_power

    if(message['TYPE'] == 'STATUS'):
        #print message
        hem_status = message

    if(message['TYPE'] == 'DCVOLT'):
        #print message
        hem_voltage = message

    if(message['TYPE'] == 'DCPOWER'):
        #print message
        hem_power = message

    if(message['TYPE'] == 'DCCURRENT'):
        #print message
        hem_current = message


hemSuperClient.subscribe(hemUpdate)

def cloudUpdate(message):
    print "Update from cloud", message.payload


def uploadToCloud():

    while True:
        hemCloudClient.sendData(hem_status)
        time.sleep(0.1)
        hemCloudClient.sendData(hem_current)
        time.sleep(0.1)
        hemCloudClient.sendData(hem_voltage)
        time.sleep(0.1)
        hemCloudClient.sendData(hem_power)

        time.sleep(60)


def readConfigFile(fileName):

    global address_mapping_scheduler
    global address_mapping_actuation
    global server_address
    global access_token

    try:
        with open(fileName, 'r') as stream:
            try:
                cfg = yaml.safe_load(stream)
                for section in cfg:
                    #print section
                    if(section == 'address_mapping_actuation'):
                        address_mapping_actuation_temp = cfg['address_mapping_actuation']
                        for i in range(len(address_mapping_actuation_temp)):
                            address_mapping_actuation.update(
                                address_mapping_actuation_temp[i])
                        #print address_mapping_actuation
                    if(section == 'address_mapping_scheduler'):
                        address_mapping_scheduler_temp = cfg['address_mapping_scheduler']
                        for i in range(len(address_mapping_scheduler_temp)):
                            address_mapping_scheduler.update(
                                address_mapping_scheduler_temp[i])
                        #print address_mapping_scheduler
                    if(section == 'access_token'):
                        access_token = cfg['access_token']
                        #print access_token
                    if(section == 'server_address'):
                        server_address = cfg['server_address']
                        #print access_token

            except yaml.YAMLError as exc:
                print "file error"
                sys.exit()

    except IOError:
        print "file error"
        sys.exit()



def main():

    time.sleep(100)
    """
    Todo:
    * Check to make sure HEMApp and its server is running before you run this app
    * Right now I'm using delay and assuming that the server will be up in 90secs
    * This problem has to be dealt with in HEMSuperClient which handles the request
    """

    # SoC Calculator Object


    logger.info('Starting cloud.py script')
    global soc
    global hemCloudClient
    global socCalc

    #Read config.yaml
    readConfigFile("cloud-config.yaml")

    """
    Todo:
    * Check to make sure all the values exist before going ahead.
    """

    #hemCloudClient to send data to Elektrifi Cloud

    hemCloudClient = HemCloudClient(access_token, server_address, address_mapping_actuation)
    hemCloudClient.subscribe(cloudUpdate)

    hemSuperClient.sendRequest("api/getdcvoltage/all")
    hemSuperClient.sendRequest("api/getnodestatus/all")
    hemSuperClient.sendRequest("api/getdccurrent/all")
    hemSuperClient.sendRequest("api/getdccharges/all")
    hemSuperClient.sendRequest("api/getdcpower/all")

    time.sleep(0.5)

    t = threading.Thread(target=uploadToCloud)
    t.start()

    while(1):

        # This sends a request to HEM every 5 seconds
        # Argument is the APIs
        hemSuperClient.sendRequest("api/getdcpower/all")
        hemSuperClient.sendRequest("api/getdcvoltage/all")
        hemSuperClient.sendRequest("api/getnodestatus/all")
        hemSuperClient.sendRequest("/api/getdccurrent/all")
        hemSuperClient.sendRequest("api/getdccharges/all")

        time.sleep(1)


if __name__ == "__main__":
    main()
